### Motivation

For consistency it's important that mailbox drivers provide the same
functionality. Inconsistency is especially bad when moving from one
store to another (say, mbox to maildir) or use them
simultaneously.

### Inconsistencies

 * [NewMailHandling](NewMailHandling)  
 * `$save_empty` only works for mbox/mmdf, POP can't support it, don't know about IMAP.  
 * trashed files: we have `$maildir_trash` for maildir, `$mh_purge` for MH but nothing for mbox/mmdf (though searches on the indicate the X-Status: header has a "D" flag for it)  
 * there are tickets requesting to support more flags like "passed" (bounced/forwarded/resent) for maildir, etc.

### Solutions

For removing empty folders, there's a long discussion on
[mutt-users](http://does-not-exist.org/mail-archives/mutt-users/msg19484.html).
Short: as it cannot be atomically, an implementation would contain a
number of race conditions that cannot be safely handled. Thinking of
MDAs as part of the delivery, it may even cause mail loss.

We should add `$mailbox_trash` and make the existing options synonyms
for it and add support for `X-Status: D` (we do support X-Status: AF
already). See [dovecot
docs](http://wiki.dovecot.org/MailboxFormat/mbox#Dovecot.27s_Metadata)
(other sources mention the same flags).

We shouldn't probably support more flags unless they work for all
mailbox formats. Inventing new flags for mbox just to support them
breaks inter-operability with other clients. Example: \#2460 is probably
invalid.
