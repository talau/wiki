**A good start for finding the right things is knowing where everything
is.**

## Organizing / managing emails

Mutt keeps emails in *folders* (collections of individual emails), which
you can use to "change" to (for viewing) or to "save / move / copy" to
in the UI, or specify in configuration files as targets of operations.
For a typical use you need some standard folders:

| folder "callsign" | location configuration in muttrc | use feature or not | purpose |
| ----------------- | -------------------------------- | ------------------ | ------- |
| "inbox" | **$spoolfile** | *always* | This is the **startup folder** assumed to contain all new email that arrives for you.|
| "mbox" or "received" | **$mbox** | **$move** | If you want to save (move) mails out of the inbox which you've read instead of deleting them right on (or cluttering up your inbox).|
| "sent" | **$record** | **$copy** | Sometimes it's good to keep a copy of sent out messages in case you need to resend them or verify discussions.
| "postponed" | **$postponed** | **$postpone** | Start new email but then pause to continue later.|
| "personal folders" | -- | -- | have several folders to keep messages separate for different uses (by topic, sender, recipient, subject).|

The "inbox" is provided by the system at a specific place. Default
location is the $MAIL shell variable. Unless mutt is compiled to assume
"inbox" in your $HOME dir, it will probably be something like
"/var/spool/mail/USER". "personal folders" have no shortcuts, you use
them by specifying the pathname each time, there is no mutt-internal
management for those. Make up your mind about a folder structure before
they get overloaded (bad overview & performance for too big folders).

All other folders can be located anywhere you can write to. However, the
recommended default for them is to reside in a **common subdir** in your
$HOME (like "~/Mail").

### Easy access for convenience: folder collections

Depending on your directory structure, pathnames for the folders can
get quite lengthy and/or cumbersome to type each time.  Mutt provides
shortcuts to save some typing, especially in the UI dialog prompts.
If you have a **common subdir* for all folders, then you can specify
it in **"$folder"**, and replace all path references to this common
subdir with "=" or "+". Example:

     in muttrc:
     set record="$HOME/Mail/sent"; set mbox="$HOME/Mail/mbox"; set postponed="$HOME/Mail/postponed"
     =>
     set folder="$HOME/Mail"; set record="+sent"; set mbox="+mbox"; set postponed="+postponed"

**Note:** do not end "folder" path with a "/", because mutt
automatically inserts one when you use "=" or "+". An extra one would
break some functionality because of "//".

This easy access works for your personal folders in "$folder", too (like
"+family" or "+cats"). It pays off when you use it in many more places
in the config and interactively, like when you have *many* personal
folders.

**IMPORTANT Note:** "+" & "=" are replaced with the **expanded value**
of **$folder** at the time used with "set" (or hook). There is **no
dynamic link** between them, so if you change **$folder** afterwards,
then the "set" defined var (or hook-pattern) keeps its static value as
defined before. Typically you set "$folder" first and all its
dependencies after that, this means: the order matters. For advanced
setups where you work with several different "$folder" values it can be
useful to change it after you used it for some relative assignments. But
there should always be *some desired* value before you use it the first
time.

### Shortcuts overview for mutt internal folders

Here are some more:

| refers to | **typical** place | mutt-var | shortcut |
| --------- | ----------------- | -------- | -------- |
| "inbox" | /var/spool/mail/USER | $spoolfile | "!" |
| $HOME dir | /home/user | no-mutt var | "~" or "$HOME" |
| folders dir | /home/user/mail | $folder | "+" or "=" |
| "mbox" | /home/user/mail/received | $mbox | "\>" |
| "sent" | /home/user/mail/sent | $record | "\<" |
| unfinished | /home/user/mail/postponed | $postponed | "+postponed" |
| personal | /home/user/mail/personal | | "+personal" |
| repeat the last for all your personal folders |
| last visited | varies | | "-"or "!!" |
| *default folder* for alias | varies | | "@alias" |
| currently open folder (\*) | varies | | "^"|

(\* Note: you need the
"current\_folder" patch from [PatchList](PatchList), or mutt 1.5.10+ for this)

Note: not all those vars are set by default, or maybe point to
"untypical" places. Make sure you set them all as you want them!

### Remote folders

Wherever you have to specify a folder, you are not limited to folders
located on your local filesystem. You may use IMAP and POP folders on
remote servers, too. Beware however, don't use it for folders that get
used automatically by mutt (like "inbox" or "sent"), because if you have
a slow network connection to the remote server, it can slow down
performance significantly.

The syntax for paths to remote folders
is:

* `imap[s]://[user[:pw]@]imapserver.example.com[:port]/path`  
* `pop[s]://[user[:pw]@]popserver.example.com[:port]/`   (no path, POP can handle only 1 mailbox)

Note: the "user", "password" and "port" parts are optional. imaps and
pops are SSL-encrypted variants of imap and pop. More about IMAP & POP
access configuration later.

"$folder" works for IMAP, too:

* in muttrc: `set folder="imaps://user@example.com"`

to be used in UI for "change folder" cmd:

* in mutt UI: `c+sent<enter>`

See also [/Actions](MuttGuide/Actions) about Macros + TAB-completion.

### Storage format

How a folder is organized technically (filesystem internally) has an
impact on performance. Like in many other cases, you have to decide
between security, stability, performance, ease of use. There exist 4
formats that mutt understands: MBOX, MAILDIR, MH, MMDF (the order
doesn't correlate with the previous qualities!). For an in depth
discussion have a look at [FolderFormat](FolderFormat) &
[MuttFaq/Maildir](MuttFaq/Maildir). If you are not
much into technicalities, leave that for later.

It is recommended, although not required, to use the same format as the
[MDA]([MailConcept](MailConcept) "wikilink"), which places the new incoming emails into
your "inbox". If you can't change the MDA-format but prefer to use some
other with mutt, just do so: mutt autodetects the mailbox formats when
reading them. When you create new folders, mutt will use the format
specified in **$mbox\_type** (you have to create them all yourself,
except "inbox", which should be provided by the system).
[br](br "wikilink") For now without further reasons, let's stick with
MBOX format, the simple "classic". Please be aware of confusion by
[OverloadedNames](OverloadedNames)!
