To use IMAP with mutt, set `spoolfile` to an [IMAP
URL](http://tools.ietf.org/html/rfc5092) of the form:

    imap[s]://[<username>[:<password>]@]<server>/[<mailbox>]

If your folders are addressed as sub-folders of your INBOX folder,
`INBOX` should be given as the `mailbox` component, otherwise leave it
empty. If you omit `password`, or indeed `username`, mutt will prompt
for this on startup.

You should also set `folder` to a similar IMAP URL, however there is no
need for authentication information here.

Here's a sample `~/.muttrc` file, for a user johndoe with the INBOX
sub-folder setup on a server mail.example.com:

    # Automatically log in to this mailbox at startup
    set spoolfile="imaps://johndoe:p455w0rd@mail.example.com/INBOX"
    # Define the = shortcut, and the entry point for the folder browser (c?)
    set folder="imaps://mail.example.com/INBOX"
    set record="=Sent"
    set postponed="=Drafts"

And here's a sample `~/.muttrc` for a user jane at another site, this
time without the INBOX sub-folder setup. Jane is prompted for her
password at startup:

    # Automatically log in to this mailbox at startup
    set spoolfile="imaps://jane@mail.demo.com/"
    # Define the = shortcut, and the entry point for the folder browser (c?)
    set folder="imaps://mail.example.com/"
    set record="=Sent"
    set postponed="=Drafts"

Once you have started mutt, you may press c? to change between your IMAP
folders. If this shows a list of filesystem folders, instead of expected
IMAP folders, then you probably need to remove the trailing INBOX from
the spoolfile and folder directives as per the second example above.

# Other useful ~/.muttrc settings:

If you applied the so useful for us IMAP users [trash\_folder
patch](http://cedricduval.free.fr/mutt/patches/#trash), also add:

``` 
 set trash="=Trash"
```

Also, some other settings that might depend on the servers
configuration:

``` 
 # activate TLS if available on the server
 set ssl_starttls=yes
 # always use SSL when connecting to a server
 set ssl_force_tls=yes
 # Don't wait to enter mailbox manually 
 unset imap_passive        
 # Automatically poll subscribed mailboxes for new mail (new in 1.5.11)
 set imap_check_subscribed
 # Reduce polling frequency to a sane level
 set mail_check=60
 # And poll the current mailbox more often (not needed with IDLE in post 1.5.11)
 set timeout=10
 # keep a cache of headers for faster loading (1.5.9+?)
 set header_cache=~/.hcache
 # Display download progress every 5K
 set net_inc=5
```

# Tuning Imap

## Caching

There're several starting points to tune mutt and increase IMAP
performance with local caching reducing network usage: mutt can be made
to fetch every item (header, message) just once and work with local
copies after that.

If local caching is not desired for good reasons, mutt features such as
attachment counting or spam detection should be avoided as they likely
will cause mutt to fetch either headers or even complete messages over
and over again.

See [/Caching](MuttGuide/Caching) for more information.

## Searching

~patterns which work on body and/or header require that *every* message
(part) has to be fetched so it can be checked for a match. If you want
to avoid prefetch, work with =h and =b instead: this leaves the search
on the server (if it supports it), but notice that it's literal string
only, no regular expression. Note that this feature is not available
before version 1.5.11.

When body caching is used, regular expression searches may not be that
expensive as they'll use the local message cache fetching only missing
messages first.
