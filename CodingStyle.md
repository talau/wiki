## Style Guide

When hacking on mutt, please make sure to keep the following
guidelines:

* global functions should have the prefix "mutt_".  All other functions should be declared "static".  
* avoid global vars where possible.  
 * If one is required, try to contain it to a single source file and declare it "static".  
 * Global vars should have the first letter of each word capitalized, and no underscores should be used (e.g., !MailGid, !LastFolder, !MailDir).  
* re-use code as much as possible. There are many "library" functions. A major cause of bloat in ELM and PINE is the tremendous duplication of code... Help keep Mutt small!  
* when adding new options, make the old behavior the default.  
* try to keep mutt as portable as possible.

## Code Indentation

`indent -nce -bls --braces-after-if-line
--brace-indent0 --case-indentation2 --indent-level2`
and cleanup

``` 
 if(! hc)
 {
   return -1;
 }
```

to

    if(! hc)
       return -1;
    etc

and break some lines which are longer than 80 chars into two manually.

## Documentation

Please document your changes. Note that there are several places where
you may have to add
documentation:

* doc/manual.xml.{head,tail} contain The Manual.  
* doc/muttrc.man.{head,tail} contain an abridged version of The Manual in nroff format (see man(7)), which deals with configuration file commands.

Configuration **variables** are documented directly in \`init.h\`. Note
that this includes documentation for possibly added format flags!

The parts of The Manual and the muttrc manual page dealing with these
variables, and the global Muttrc, are generated automatically from that
documentation. To start this process, type "\`make update-doc\`" in the
top-level source directory.

Note that you may have to update the makedoc utility (makedoc.c) when
adding new data types to \`init.h\`.

More precisely, variable name, type, and default value are directly
extracted from the initializer for the !MuttVars array. Documentation
is expected in special comments which **follow** the initializer. For a
line to be included with the documentation, it must (after, possibly,
some white space) begin with either "/\*\*" or "\*\*". Any following
white space is ignored. The rest of the line is expected to be plain
text, with some formatting instructions roughly similar to
!\[ntg\]roff:

* \fI switches to italics  
* \fB switches to boldface  
* \fP switches to normal display  
* \(as can be used to represent an asterisk (*).  This is intended to help avoiding character sequences such as /* or */ inside comments.  
* \(rs can be used to represent a backslash (\).  This is intended to help avoiding problems when trying to represent any of the \ sequences used by makedoc.  
* .dl on a line starts a "definition list" environment (name taken from HTML) where terms and definitions alternate.  
* .dt marks a term in a definition list.  
* .dd marks a definition in a definition list.  
* .de on a line finishes a definition list environment.  
* .ts on a line starts a "verbose tscreen" environment (name taken from SGML).  Please try to keep lines inside such an environment short; a length of abut 40 characters should be ok.  This is necessary to avoid a really bad-looking muttrc (5) manual page.  
* .te on a line finishes this environment.  
* .pp on a line starts a paragraph.  
* $word will be converted to a reference to word, where appropriate. Note that $$word is possible as well. Use $$$ to get a literal $ without making a reference.  
* '. ' in the beginning of a line expands to two space characters. This is used to protect indentations in tables.

Do **not** use any other SGML or nroff formatting instructions here!
