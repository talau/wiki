### How to make PGP/GPG work?

Verify that you've installed PGP properly and your mutt is compiled with
PGP support:

`mutt -v`

RTFM about the **$pgp\_\*** vars, **pgp-hook**, use
`source <path-to-manual.txt>/samples/pgpX.rc`
where you replace the "X" with the version of PGP you are using. You can
find there a file for GPG, too.

Read [The GNU Privacy Handbook](http://www.gnupg.org/gph/en/manual.html)
as a general introduction to GnuPG.

### How to make *oldstyle / classic / traditional / inline* PGP work?

Three ways to read such messages, in decreasing comfort:

  - Apply **ddm.pgp-always-check-traditional.2** (for Mutt 1.4.x) or
    **ddm.pgp-auto-decode** (for Mutt 1.5.x) patch by Derek D. Martin at
    <http://www.pizzashack.org/mutt/> (see [PatchList](PatchList)).

  - With recent Mutts \>= 1.5.7, use the **<check-traditional-pgp>**
    function automatically inside a **message-hook**, setting this one
    line in
    muttrc:

    ``` 
  message-hook '!(~g|~G) ~b"^-----BEGIN\ PGP\ (SIGNED\ )?MESSAGE"' "exec check-traditional-pgp"
    ```

**Note:** This doesn't work so well with inline signatures within MIME digests.

  - Use the **pgp\_auto\_decode** option, which is very similar to the
    message-hook method but also works within MIME digests.
  - Use the **<check-traditional-pgp>** function manually, typing **ESC
    P** key.

**Note:** The way \#2 (**message-hook**) **was** triggering an
infinite loop bug in old Mutts. Bug solved long ago: Upgrade.

**Note:** Avoid any random advice to use procmail recipe to change
inline text to app/pgp so Mutt can deal with the message. This is an
old dirty hack that at first may have seemed to work, but solving
one problem created three others. Today: Avoid at all costs.

Since some coders of mutt established RFCs for usage of PGP secured
eMail with MIME (2015, 3156, see [RelevantStandards](RelevantStandards)) to make lives easier
for users (and clients), avoid creating old style PGP email, let mutt do
it right.

If for some *stuck with old software*-reason you insist on producing
them, adjust **$pgp\_create\_traditional** to your
needs.

### Other MUAs (mail-programs) can't handle mutt's PGP-stuff being attachments?

That's a pity, ... really, seriously. Even though mutt has a head-start
when it comes to RFC compliance with MIME + PGP, by now other MUA coders
should have noticed that there is a standardized way to deal with it,
more useful than the old inline stuff.

If you keep getting complaints of OE users not able to read your
message, try:

`reply-hook "~h 'Outlook Express'" set crypt_autosign = no`  
`reply-hook . set crypt_autosign = yes`

### I typed wrong the passphrase, but mutt still keeps using it?

Maybe your PGP commands return bad exit codes for errors. See "?" for
how to **forget-passphrase**.

### Does Mutt do S/MIME?

There is support for S/MIME in the 1.5.4 (?) and higher.

There is an HTML guide at <http://equiraptor.com/smime_mutt_how-to.html>

Note that openssl-0.9.7d fails to encrypt your messages because of a bug
in it:
<http://www.mail-archive.com/openssl-dev@openssl.org/msg17377.html>

### I'm using gpg to encrypt messages and can't read my own postings

gpg can encrypt messages to several people at once. Add your keyid and
you will be able to read messages in your $record folder.

The easiest way is to add

`encrypt-to `*`your-keyid`*

to ~/.gnupg/gpg.conf (~/.gnupg/options in gpg 1.0). You can
alternatively add it in Mutt's crypt\_\* options. But **BEWARE** of the
security implications this has. Basically, this adds another
vulnerability to the encrypted message. If either sender **or** the
receiver key is compromised, an attacker can read the message.

Another even less secure possibility is to save your local copy
unencrypted. See mutt's $fcc\_clear.

So there are two possibilities: Encrypt with your key too or save a
cleartext copy. The safest option is to not store a local copy or to
live with the fact that you cannot read the message.

### Which keyserver should I use?

**subkeys.pgp.net** is a rotation server that includes only the new sks
servers. The old public key servers often screw up keys by dropping
subkeys or multiplying user ids.

### How can I retrieve PGP-keys automatically?

That is a gpg setting. (~/.gnupg/gpg.conf)

    keyserver subkeys.pgp.net [or any other server]
    keyserver-options auto-key-retrieve
